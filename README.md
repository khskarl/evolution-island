# Evolution Island

A zero-player game inspired by [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life). Intended as a demonstration of the [Amethyst Engine](https://amethyst.rs/).

[Read here for project MVP details](https://community.amethyst-engine.org/t/demo-game-evolution-island-mvp/487)

## To play
Ensure you have Cargo installed ([use rustup if you don't](https://rustup.rs/)), and run the following:
```
cargo run
```
